import Vue from 'vue'
import App from './App.vue'
import Vuetify from 'vuetify'
import axios from 'axios'
import Vuex from 'vuex'
import router from './router'
import VueChartkick from 'vue-chartkick'
import Chart from 'chart.js'
import EventHub from 'vue-event-hub'
import VueQuillEditor from 'vue-quill-editor'
import VueClip from 'vue-clip'
import {vueImgPreview} from 'vue-img-preview'
import * as VueGoogleMaps from 'vue2-google-maps'



// require styles
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'
Vue.use(VueQuillEditor, /* { default global options } */)

Vue.use(EventHub)
Vue.use(VueChartkick, {adapter: Chart})
Vue.use(Vuex)
Vue.use(Vuetify)
Vue.use(VueClip)

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyCM-gFc5l8At0k-Go1u4uW3x0-oiAyq04A',
    libraries: 'places', // This is required if you use the Autocomplete plugin
    // OR: libraries: 'places,drawing'
    // OR: libraries: 'places,drawing,visualization'
    // (as you require)

    //// If you want to set the version, you can do so:
    // v: '3.26',
  },

  //// If you intend to programmatically custom event listener code
  //// (e.g. `this.$refs.gmap.$on('zoom_changed', someFunc)`)
  //// instead of going through Vue templates (e.g. `<GmapMap @zoom_changed="someFunc">`)
  //// you might need to turn this on.
  // autobindAllEvents: false,

  //// If you want to manually install components, e.g.
  //// import {GmapMarker} from 'vue2-google-maps/src/components/marker'
  //// Vue.component('GmapMarker', GmapMarker)
  //// then disable the following:
  // installComponents: true,
})

Vue.component('vue-img-preview', vueImgPreview)

import 'vuetify/dist/vuetify.min.css'

import VuexStore from './store'
const store = new Vuex.Store(VuexStore)
let {state} = VuexStore.modules.system

state.$api = axios.create({
  //baseURL: 'https://api.bigoferta.com/'
  baseURL: 'https://app.pushin.com.br/'
})


//config Authorization
state.$api.interceptors.request.use( config => {
    config.headers.common['Authorization'] = state.token
    console.log(config)
    return config
}, error => {
    return Promise.reject(error)
})

/*
//config Authorization
state.$api.interceptors.response.use( resp => {
    return resp
}, error => {
    if( error.response.status == 403 )
        sessionStorage.removeItem("token")
    return Promise.reject(error)
})
*/

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
