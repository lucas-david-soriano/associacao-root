import store from '../store/index.js'
function requireAuth (to, from, next)
{
  let jwt   = sessionStorage.getItem("token")
  let user  = JSON.parse(sessionStorage.getItem("user"))
  store.modules.system.state.token = jwt
  store.modules.system.state.user  = user
  if( !jwt ) return next("/login")
  next()
}
export default requireAuth
