export default {


  listaEmpresa({commit, getters, rootState})
	{
    return new Promise( (res,rej) => {
          getters.getApi.get(`v1/empresas`).then( resp => {
            commit('SET_EMPRESAS', resp.data)
            res(resp.data)
          })
    })
  },

  listaEmpresaAssociacao({commit, getters, rootState}, data)
	{
    return new Promise( (res,rej) => {
          getters.getApi.get(`v1/empresas/${data}`).then( resp => {
            commit('SET_EMPRESAS', resp.data)
            res(resp.data)
          })
    })
  },

  alteraEmpresa({commit, getters, rootState}, data)
	{
    return new Promise( (res,rej) => {
        getters.getApi.put(`v1/empresas`, data).then( resp => {
          res(resp.data)
        })
    })
  },

  deletaEmpresa({commit, getters, rootState}, data)
	{       
    return new Promise( (res,rej) => {
        getters.getApi.delete(`v1/empresas/${data}`).then( resp => {
          res(resp.data)
        })
    })
  },

  listaCategoriaEmpresa ({getters, commit, rootState}, data) {
  
    return new Promise( (res,rej) => {
      getters.getApi.get(`v1/categorias`, data).then( resp => {
        commit('SET_CATEGORIASEMPRESA', resp.data.data)
        res(resp.data)
      })
    })
  },

  listaCategorias ({getters, commit, rootState}, data) {
  
    return new Promise( (res,rej) => {
      getters.getApi.get(`v1/categorias`, data).then( resp => {
        commit('SET_CATEGORIAS', resp.data)
        res(resp.data)
      })
    })
  },

  cadastroCategoria ({getters, commit, rootState}, data) {
    console.log(data)
    return new Promise( (res,rej) => {
      getters.getApi.post(`v1/categorias`, data).then( resp => {
        res(resp.data)
      })
    })
  },

  alteraCategoria ({getters, commit, rootState}, data) {
    console.log(data)
    return new Promise( (res,rej) => {
      getters.getApi.put(`v1/categorias`, data).then( resp => {
        res(resp.data)
      })
    })
  },

  deletaCategoria ({getters, commit, rootState}, data) {
    console.log(data)
    return new Promise( (res,rej) => {
      getters.getApi.delete(`v1/categorias/${data}`).then( resp => {
        res(resp.data)
      })
    })
  },


  cadastroEmpresas ({getters, commit, rootState}, data) {
    console.log(data)
    return new Promise( (res,rej) => {
      getters.getApi.post(`v1/empresas`, data).then( resp => {
        res(resp.data)
      })
    })
  }
}
